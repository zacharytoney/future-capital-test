const stocks = require('./stocks.json');
const prices = [7,1,5,3,6,4];
// Part 1
function sumOfPortfolio(stocks){
    // This is just straight up adding all the prices together
    // So for loop to run through it all
    var sumOfPortfolio = 0;
    for(stock in stocks){
        if(typeof(stocks[stock].close) !== 'undefined'){
            sumOfPortfolio+=stocks[stock].close;
        }
    }
    return sumOfPortfolio.toFixed(2);
}
// Part 2
 var maxProfit = function(prices) {
    // We basically use a left and right pointer to keep track of max profit
    // Also known as a sliding window method
    // So the time complexity of this would be O(n)
    // Memory complexity would be O(1)
    // Comments are by me Zachary Toney
    var lPointer = 0;//left = buy
    var rPointer = 1;//right = sell
    var maxProfit = 0;
    while(rPointer < prices.length){
        if(prices[lPointer] < prices[rPointer]){
            if(maxProfit < (prices[rPointer] - prices[lPointer])){
                maxProfit = (prices[rPointer] - prices[lPointer]);
            }
        }
        else{
            lPointer=rPointer;
        }
        rPointer+=1;
    }
    return maxProfit;
};
console.log("Part 1 | Sum of portfolio: "+sumOfPortfolio(stocks));
console.log("Part 2 | Max Profit: "+maxProfit(prices));
